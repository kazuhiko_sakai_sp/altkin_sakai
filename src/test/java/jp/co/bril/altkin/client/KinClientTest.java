package jp.co.bril.altkin.client;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;

import jp.co.bril.altkin.util.AbstractTestClass;

public class KinClientTest extends AbstractTestClass{

	@ClassRule public static TestWatcher classrule = printClassName();
	@Rule public TestWatcher rule = printMethodName();

	@Test
	public void 勤怠登録テスト() throws Throwable {
		KinClient client = new KinClient("202109SK", "", "", "09:45");
		try {
			String bkid = client.executeLogic();
			System.out.println(bkid);
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}
}
