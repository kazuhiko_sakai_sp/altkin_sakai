package jp.co.bril.altkin;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.TestWatcher;
import org.junit.runner.RunWith;

import jp.co.bril.altkin.util.AbstractTestClass;
import jp.co.bril.altkin.util.QueryJoiner;

//@RunWith(Enclosed.class)
public class QueryJoinerTest {
	public static class ベーシックなテスト extends AbstractTestClass {
		@ClassRule public static TestWatcher classrule = printClassName();
		@Rule public TestWatcher rule = printMethodName();

		@Test
		public void MapEntryをaddするテスト() throws Throwable {
			Map.Entry<String, Object> entry = new Map.Entry<String, Object>() {
				@Override public Object setValue(Object value) { throw new UnsupportedOperationException(); }
				@Override public String getKey() { return "key"; }
				@Override public Object getValue() { return "value"; }
			};

			String actual = new QueryJoiner().add(entry).toString();
			String expected = "key=value";
			assertEquals(actual, expected);
		}

		@Test
		public void QueryJoiner_Queryをaddするテスト() throws Throwable {
			QueryJoiner.Query entry = QueryJoiner.query("key", "value");
			String actual = new QueryJoiner().add(entry).toString();
			String expected = "key=value";
			assertEquals(actual, expected);
		}

		@Test
		public void keyとvalueをaddするテスト() throws Throwable {
			String actual = new QueryJoiner().add("key", "value").toString();
			String expected = "key=value";
			assertEquals(actual, expected);
		}

		@Test
		public void 結合子を変更するテスト() throws Throwable {
			String actual = new QueryJoiner(":", ",").add("foo", "FOO").add("bar", "BAR").toString();
			String expected = "foo:FOO,bar:BAR";
			assertEquals(actual, expected);
		}

		@Test
		public void mapをaddAllするテスト() throws Throwable {
			Map<String, Object> map = new LinkedHashMap<>();
			map.put("foo", "FOO");
			map.put("bar", "BAR");

			String actual = new QueryJoiner().addAll(map).toString();
			String expected = "foo=FOO&bar=BAR";
			assertEquals(actual, expected);
		}

		@Test
		public void 複合的なテスト() throws Throwable {
			Map<String, Object> map = new LinkedHashMap<>();
			map.put("foo", "FOO");
			map.put("bar", "BAR");

			String actual = new QueryJoiner(":", ",").addAll(map).add("キー", "バリュー")
								.sorted(QueryJoiner.ORDER_BY.LEX).encodeURL(Charset.forName("UTF-8")).toString();
			String expected = "bar:BAR,foo:FOO," + URLEncoder.encode("キー", "UTF-8") + ":" + URLEncoder.encode("バリュー", "UTF-8");
			assertEquals(actual, expected);
		}
	}

	@RunWith(Theories.class)
	public static class URLエンコードテスト extends AbstractTestClass {
		@ClassRule public static TestWatcher classrule = printClassName();
		@Rule public TestWatcher rule = printMethodName();

		@DataPoints public static Charset[] charset = { Charset.forName("UTF-8"), Charset.forName("MS932") };

		@Theory
		public void エンコードパターン(Charset charset) throws Throwable {
			System.out.println(" charset: " + charset.displayName());

			String actual = new QueryJoiner().add("キー", "バリュー").encodeURL(charset).toString();
			String expected = URLEncoder.encode("キー", charset.displayName()) + "=" + URLEncoder.encode("バリュー", charset.displayName());
			assertEquals(actual, expected);
		}
	}

	@RunWith(Theories.class)
	public static class ソートテスト extends AbstractTestClass {
		@ClassRule public static final TestWatcher classrule = printClassName();
		@Rule public final TestWatcher rule = printMethodName();

		private static final class TestPattern {
			final String name;
			final Comparator<Map.Entry<String, Object>> comparator;
			final String expected;
			public TestPattern(String name, Comparator<Map.Entry<String, Object>> comparator, String expected) {
				this.name = name;
				this.comparator = comparator;
				this.expected = expected;
			}
		}

		@DataPoints public static TestPattern[] pattern = {
			new TestPattern("ORDER_BY.REGIST"          , QueryJoiner.ORDER_BY.REGIST          , "a=A&c=C&b=B"),
			new TestPattern("ORDER_BY.REGIST_REVERSED" , QueryJoiner.ORDER_BY.REGIST_REVERSED , "b=B&c=C&a=A"),
			new TestPattern("ORDER_BY.LEX"             , QueryJoiner.ORDER_BY.LEX             , "a=A&b=B&c=C"),
			new TestPattern("ORDER_BY.LEX_REVERSED"    , QueryJoiner.ORDER_BY.LEX_REVERSED    , "c=C&b=B&a=A"),
		};

		@Theory
		public void ソートパターン(TestPattern pattern) throws Throwable {
			System.out.println(" orderBy: " + pattern.name);
			String actual = new QueryJoiner().add("a", "A").add("c", "C").add("b", "B").sorted(pattern.comparator).toString();
			assertEquals(actual, pattern.expected);
		}
	}

	public static class QueryCollectorTest extends AbstractTestClass {
		@ClassRule public static final TestWatcher classrule = printClassName();
		@Rule public final TestWatcher rule = printMethodName();

		@Test
		public void Map_entrySetのStreamを文字列にするテスト() throws Throwable {
			Map<String, String> map = new LinkedHashMap<>();
			map.put("foo", "FOR");
			map.put("bar", "BAR");
			map.put("baz", "BAZ");
			String actual = map.entrySet().stream().collect(QueryJoiner.joining());
			assertEquals(actual, "foo=FOR&bar=BAR&baz=BAZ");
		}

		@Test
		public void Map_entrySetのStreamを文字列にするテスト２() throws Throwable {
			Map<String, String> map = new LinkedHashMap<>();
			map.put("foo", "FOR");
			map.put("bar", "BAR");
			map.put("baz", "BAZ");
			String actual = map.entrySet().stream()
					.map(QueryJoiner::query)
					.map(field -> field.key("x-" + field.getKey()))
					.collect(QueryJoiner.joining(": ", ", "));
			assertEquals(actual, "x-foo: FOR, x-bar: BAR, x-baz: BAZ");
		}

		@Test
		public void Map_entrySetのStreamを文字列にするテスト_区切り文字指定() throws Throwable {
			Map<String, String> map = new LinkedHashMap<>();
			map.put("foo", "FOR");
			map.put("bar", "BAR");
			map.put("baz", "BAZ");
			String actual = map.entrySet().stream().collect(QueryJoiner.joining(":", ","));
			assertEquals(actual, "foo:FOR,bar:BAR,baz:BAZ");
		}
	}
}
