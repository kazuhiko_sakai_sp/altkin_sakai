package jp.co.bril.altkin.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.function.Function;

import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class AbstractTestClass {

	protected static TestWatcher printClassName() {
		return new TestWatcher() {
			@Override protected void starting(Description d) { System.out.println("----- " + d.getTestClass().getSimpleName() + " -----"); }
		};
	}

	protected static TestWatcher printMethodName() {
		return new TestWatcher() {
			@Override protected void starting(Description d) { System.out.println("[" + d.getMethodName() + "]"); }
		};
	};

	protected static <T> void assertEquals(T actual, T expected) throws Throwable {
		assertThat(actual, expected, CoreMatchers::is);
	}

	protected static <T> void assertThat(T actual, T expected, Function<T, Matcher<T>> matcher) throws Throwable {
		try {
			System.out.println("  actual: " + actual);
			System.out.println("expected: " + expected);
			MatcherAssert.assertThat(actual, matcher.apply(expected));
			System.out.println("  assert: ok");
		} catch(Throwable e) {
			System.out.println("  assert: fail" + System.lineSeparator() + toString(e));
			throw e;
		} finally {
			System.out.println();
		}
	}

	private static String toString(Throwable e) {
		try(ByteArrayOutputStream out = new ByteArrayOutputStream(); PrintStream s = new PrintStream(out)) {
			e.printStackTrace(s);
			return IOUtils.toString(out.toByteArray(), System.getProperty("file.encoding"));
		} catch (IOException e1) {
			throw new RuntimeException("print exception failed.", e1);
		}
	}

}