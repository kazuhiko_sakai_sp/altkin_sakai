package jp.co.bril.altkin.util;

import java.nio.charset.Charset;

public class Constants {
	public static final Charset CHARSET = Charset.forName("EUC-JP");
	public static final String BASEURL = "http://bril.co.jp/PHP/kks/";
}
