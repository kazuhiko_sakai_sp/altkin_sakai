package jp.co.bril.altkin.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QueryJoiner {
	private static final String DEFAULT_PAIR_DELIMITER = "=";
	private static final String DEFAULT_FIELD_DELIMITER = "&";
	private static final int DEFAULT_CAPACITY = 10;
	private final List<Query> queries;
	private final String pairDelimiter;
	private final String fieldDelimiter;
	private Charset urlEncodingCharset;
	private Comparator<Map.Entry<String, Object>> orderBy;

	/** クエリの並び順 */
	public static final class ORDER_BY {
		/** クエリの並び順: 登録順 */
		public static final Comparator<Map.Entry<String, Object>> REGIST = (a, b) -> 1;
		/** クエリの並び順: 登録順の逆順 */
		public static final Comparator<Map.Entry<String, Object>> REGIST_REVERSED = (a, b) -> -1;
		/** クエリ並び順: キーの辞書順 */
		public static final Comparator<Map.Entry<String, Object>> LEX = (a, b) -> a.getKey().compareTo(b.getKey());
		/** クエリ並び順: キーの辞書順の逆順 */
		public static final Comparator<Map.Entry<String, Object>> LEX_REVERSED = (a, b) -> b.getKey().compareTo(a.getKey());
	}

	public QueryJoiner() {
		this(DEFAULT_PAIR_DELIMITER, DEFAULT_FIELD_DELIMITER, DEFAULT_CAPACITY);
	}

	public QueryJoiner(String pairDelimiter, String fieldDelimiter) {
		this(pairDelimiter, fieldDelimiter, DEFAULT_CAPACITY);
	}

	public QueryJoiner(String pairDelimiter, String fieldDelimiter, int initialCapacity) {
		this.queries = new ArrayList<>(initialCapacity);
		this.pairDelimiter = pairDelimiter;
		this.fieldDelimiter = fieldDelimiter;
	}

	public QueryJoiner add(Map.Entry<String, ?> field) {
		this.queries.add(query(field));
		return this;
	}

	public QueryJoiner add(String key, Object value) {
		this.queries.add(query(key, value));
		return this;
	}

	public QueryJoiner addAll(Map<String, ?> map) {
		map.entrySet().stream().map(entry -> query(entry)).forEach(this.queries::add);
		return this;
	}

	public QueryJoiner merge(QueryJoiner other) {
		this.queries.addAll(other.queries);
		return this;
	}

	public boolean isEmpty() {
		return this.queries.isEmpty();
	}

	public QueryJoiner sorted(Comparator<Map.Entry<String, Object>> orderBy) {
		this.orderBy = orderBy;
		return this;
	}

	public QueryJoiner encodeURL(Charset charset) {
		this.urlEncodingCharset = charset;
		return this;
	}

	@Override
	public String toString() {
		Stream<Query> stream = queries.stream();
		if (Objects.nonNull(this.orderBy)) {
			stream = stream.sorted(this.orderBy);
		}
		if (Objects.nonNull(this.urlEncodingCharset)) {
			stream = stream.map(this::encodeURL);
		}
		return stream.map(this::joinField).collect(Collectors.joining(this.fieldDelimiter));
	}

	private Query encodeURL(Map.Entry<String, ?> field) {
		try {
			String encodedKey = URLEncoder.encode(field.getKey(), this.urlEncodingCharset.name());
			String encodedValue = URLEncoder.encode(field.getValue().toString(), this.urlEncodingCharset.name());
			return query(encodedKey, encodedValue);
		} catch (UnsupportedEncodingException e) {
			// URLEncoder.encode(s, enc) は内部で Charset.forName(enc) している. enc に Charset.name を渡している以上, 通常発生し得ない.
			throw new AssertionError("Illegal charset: " + this.urlEncodingCharset.name(), e);
		}
	}

	private String joinField(Map.Entry<String, ?> field) {
		return new StringBuilder().append(field.getKey()).append(this.pairDelimiter).append(field.getValue().toString()).toString();
	}

	public static Query query(Map.Entry<String, ?> entry) {
		return new Query(entry);
	}

	public static Query query(String key, Object value) {
		return new Query(key, value);
	}

	/**
	 * Map.Entry の stream の Collector.
	 *
	 * @author Tatsuya
	 *
	 */
	private static class QueryCollector implements Collector<Map.Entry<String, ?>, QueryJoiner, String> {
		private final String pairDelimiter, fieldDelimiter;

		public QueryCollector() {
			this(DEFAULT_PAIR_DELIMITER, DEFAULT_FIELD_DELIMITER);
		}

		public QueryCollector(String pairDelimiter, String fieldDelimiter) {
			this.pairDelimiter = pairDelimiter;
			this.fieldDelimiter = fieldDelimiter;
		}

		@Override
		public Supplier<QueryJoiner> supplier() {
			return () -> new QueryJoiner(pairDelimiter, fieldDelimiter);
		}

		@Override
		public BiConsumer<QueryJoiner, Map.Entry<String, ?>> accumulator() {
			return QueryJoiner::add;
		}

		@Override
		public BinaryOperator<QueryJoiner> combiner() {
			return QueryJoiner::merge;
		}

		@Override
		public Function<QueryJoiner, String> finisher() {
			return QueryJoiner::toString;
		}

		@Override
		public Set<Characteristics> characteristics() {
			return Collections.emptySet();
		}
	}

	public static Collector<Map.Entry<String, ?>, QueryJoiner, String> joining() {
		return new QueryCollector();
	}

	public static Collector<Map.Entry<String, ?>, QueryJoiner, String> joining(String pairDelimiter, String fieldDelimiter) {
		return new QueryCollector(pairDelimiter, fieldDelimiter);
	}

	/**
	 * Map.EntryのQueryJoiner用内部実装.
	 * keyはString, valueはObject固定とする.
	 *
	 * @author Tatsuya
	 */
	public static class Query implements Map.Entry<String, Object> {
		private String key;
		private Object value;

		private Query(String key, Object value) {
			this.key = key;
			this.value = value;
		}

		private Query(Map.Entry<String, ?> entry) {
			this.key = entry.getKey();
			this.value = entry.getValue();
		}

		@Override
		public String getKey() {
			return this.key;
		}

		public String setKey(String key) {
			String old = this.key;
			this.key = key;
			return old;
		}

		public Query key(String key) {
			this.key = key;
			return this;
		}

		@Override
		public Object getValue() {
			return this.value;
		}

		@Override
		public Object setValue(Object value) {
			Object old = this.value;
			this.value = value;
			return old;
		}

		public Query value(Object value) {
			this.value = value;
			return this;
		}

		public Query update(String key, Object value) {
			this.key = key;
			this.value = value;
			return this;
		}
	}
}
