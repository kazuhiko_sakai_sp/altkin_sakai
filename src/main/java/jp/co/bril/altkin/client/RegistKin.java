package jp.co.bril.altkin.client;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import jp.co.bril.altkin.util.Constants;
import jp.co.bril.altkin.util.QueryJoiner;

public class RegistKin {
    private Logger logger = LoggerFactory.getLogger(RegistKin.class);
    private static final String PATH_REGIST_KIN = "WMU030/WMU030main.php";

    private Map<String, String> requestParameters = new LinkedHashMap<>();
    private Map<String, String> cookies = new LinkedHashMap<>();

    /**
     * コンストラクタ
     *
     * @param requestParameters リクエストパラメータ
     */
    public RegistKin (Map<String, String> requestParameters) {
        this.requestParameters = requestParameters;
        logger.debug("constructor called. requestParameters:{}", requestParameters);
    }

    /**
     * 勤怠情報を登録する
     * @param requestCookies
     * @return 登録結果メッセージ
     * @throws IOException
     */
    public String regist(Map<String, String> requestCookies) throws IOException {
        Connection connection = Jsoup
                .connect(Constants.BASEURL + PATH_REGIST_KIN)
                .cookies(requestCookies)
                .data(requestParameters)
                .data("ad_rsn", "")
                .data("prog1", "0")
                .data("prog2", "0")
                .data("prog3", "0")
                .data("prog4", "0")
                .data("prog5", "0")
                .method(Connection.Method.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("request:\n{}\n", requestDump(connection.request()));
        }
        Connection.Response response = connection.execute();
        this.cookies.putAll(response.cookies());
        Document body = response.parse();
        String message = body.selectFirst("input[name=bkid]").attr("value");
        logger.debug("login success. message:{}", message);
        return message;
    }

    private static String requestDump(Connection.Request request) {
        String method = request.method().toString();
        String url = request.url().toString();
        String header = request.multiHeaders().entrySet().stream()
                .map(field -> field.getKey() + ": " + String.join(", ", field.getValue()))
                .collect(Collectors.joining("\n"));
        String body = request.data().stream()
                .map(field -> QueryJoiner.query(field.key(), field.value()))
                .collect(QueryJoiner.joining());

        return new StringJoiner("\n")
                .add(method + " " + url)
                .add(header)
                .add("")
                .add(body.isEmpty() ? "(no body)" : body).toString();
    }

    public Map<String, String> getCookies() {
        return cookies;
    }
}
