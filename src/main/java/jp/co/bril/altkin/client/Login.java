package jp.co.bril.altkin.client;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import jp.co.bril.altkin.util.Constants;
import jp.co.bril.altkin.util.QueryJoiner;

public class Login {
    private Logger logger = LoggerFactory.getLogger(KinClient.class);
    private static final String PATH_LOGIN = "WMU020/WMU020main.php";

    private String id;
    private String password;
    private String caid;
    private Map<String, String> cookies = new LinkedHashMap<>();

    /**
     * コンストラクタ
     *
     * @param id ユーザID
     * @param password パスワード
     * @param caid 会社ID
     */
    public Login(String id, String password, String caid) {
        this.id = id;
        this.password = password;
        this.caid = caid;
        logger.debug("constructor called. id:{}, password:{}, caid:{}", id, password, caid);
    }

    /**
     * 勤怠システムにログインする
     *
     * @return ログインID
     * @throws MalformedURLException if the request URL is not a HTTP or HTTPS URL, or is otherwise malformed
     * @throws HttpStatusException if the response is not OK and HTTP response errors are not ignored
     * @throws UnsupportedMimeTypeException if the response mime type is not supported and those errors are not ignored
     * @throws SocketTimeoutException if the connection times out
     * @throws IOException on error
     */
    public String login() throws IOException {
        Connection connection = Jsoup
                .connect(Constants.BASEURL + PATH_LOGIN)
                .data("cnt", "0")
                .data("bkid", "")
                .data("loginid", this.id)
                .data("pswd", this.password)
                .data("caid", this.caid)
                .method(Connection.Method.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("request:\n{}\n", requestDump(connection.request()));
        }
        Connection.Response response = connection.execute();
        this.cookies.putAll(response.cookies());
        Document body = response.parse();
        String bkid = body.selectFirst("input[name=bkid]").attr("value");
        logger.debug("login success. bkid:{}", bkid);
        return bkid;
    }

    private static String requestDump(Connection.Request request) {
        String method = request.method().toString();
        String url = request.url().toString();
        String header = request.multiHeaders().entrySet().stream()
                .map(field -> field.getKey() + ": " + String.join(", ", field.getValue()))
                .collect(Collectors.joining("\n"));
        String body = request.data().stream()
                .map(field -> QueryJoiner.query(field.key(), field.value()))
                .collect(QueryJoiner.joining());

        return new StringJoiner("\n")
                .add(method + " " + url)
                .add(header)
                .add("")
                .add(body.isEmpty() ? "(no body)" : body).toString();
    }

    public Map<String, String> getCookies() {
        return cookies;
    }
}
