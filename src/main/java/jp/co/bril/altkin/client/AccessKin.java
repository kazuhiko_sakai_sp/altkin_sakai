package jp.co.bril.altkin.client;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import jp.co.bril.altkin.util.Constants;
import jp.co.bril.altkin.util.QueryJoiner;

public class AccessKin {
    private Logger logger = LoggerFactory.getLogger(AccessKin.class);
    private static final String PATH_ACCESS_KIN = "WMU030/WMU030main.php";

    private Map<String, String> cookies = new LinkedHashMap<>();

    /**
     * コンストラクタ
     */
    public AccessKin() {
        logger.debug("constructor called.");
    }

    public Map<String, String> access(Map<String, String> requestCookies) throws IOException {
        Connection connection = Jsoup
                .connect(Constants.BASEURL + PATH_ACCESS_KIN)
                .cookies(requestCookies)
                .method(Connection.Method.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("request:\n{}\n", requestDump(connection.request()));
        }
        Connection.Response response = connection.execute();
        this.cookies.putAll(response.cookies());
        Document body = response.parse();

        Map<String, String> responseData = new LinkedHashMap<String, String>();
        responseData.put("BRIL_LOGIN_ID", body.selectFirst("input[name=BRIL_LOGIN_ID]").attr("value"));
        responseData.put("BRIL_SYSTEM_ID", body.selectFirst("input[name=BRIL_SYSTEM_ID]").attr("value"));
        responseData.put("BRIL_SEARCH_ID", body.selectFirst("input[name=BRIL_SEARCH_ID]").attr("value"));
        responseData.put("BRIL_SEARCH_SYSTEM_ID", body.selectFirst("input[name=BRIL_SEARCH_SYSTEM_ID]").attr("value"));
        responseData.put("appdate", body.selectFirst("input[name=appdate]").attr("value"));
        responseData.put("Nowktm1st", body.selectFirst("input[name=Nowktm1st]").attr("value"));
        responseData.put("Nowktm1en", body.selectFirst("input[name=Nowktm1en]").attr("value"));
        responseData.put("Nowktm2st", body.selectFirst("input[name=Nowktm2st]").attr("value"));
        responseData.put("Nowktm2en", body.selectFirst("input[name=Nowktm2en]").attr("value"));
        responseData.put("Nowktm3st", body.selectFirst("input[name=Nowktm3st]").attr("value"));
        responseData.put("Nowktm3en", body.selectFirst("input[name=Nowktm3en]").attr("value"));
        responseData.put("Nowktm4st", body.selectFirst("input[name=Nowktm4st]").attr("value"));
        responseData.put("Nowktm4en", body.selectFirst("input[name=Nowktm4en]").attr("value"));
        responseData.put("Nowktm5st", body.selectFirst("input[name=Nowktm5st]").attr("value"));
        responseData.put("Nowktm5en", body.selectFirst("input[name=Nowktm5en]").attr("value"));
        responseData.put("Strtm", body.selectFirst("input[name=Strtm]").attr("value"));
        responseData.put("Enrtm", body.selectFirst("input[name=Enrtm]").attr("value"));
        responseData.put("PlaceId", body.selectFirst("input[name=PlaceId]").attr("value"));
        responseData.put("monstnsch", body.selectFirst("input[name=monstnsch]").attr("value"));
        responseData.put("AppFlg", body.selectFirst("input[name=AppFlg]").attr("value"));
        responseData.put("Count", body.selectFirst("input[name=Count]").attr("value"));
        responseData.put("savecls", body.selectFirst("input[name=savecls]").attr("value"));
        responseData.put("brk_gokei", body.selectFirst("input[name=brk_gokei]").attr("value"));
        responseData.put("PageFlg1", body.selectFirst("input[name=PageFlg1]").attr("value"));
        responseData.put("PageFlg2", body.selectFirst("input[name=PageFlg2]").attr("value"));
        responseData.put("PageFlg3", body.selectFirst("input[name=PageFlg3]").attr("value"));
        responseData.put("PageFlg4", body.selectFirst("input[name=PageFlg4]").attr("value"));
        responseData.put("PageFlg5", body.selectFirst("input[name=PageFlg5]").attr("value"));
        responseData.put("crad_st_flg", body.selectFirst("input[name=Count]").attr("value"));
        responseData.put("crad_en_flg", body.selectFirst("input[name=Count]").attr("value"));
        responseData.put("in_work", body.selectFirst("input[name=in_work]").attr("value"));
        responseData.put("out_work", body.selectFirst("input[name=out_work]").attr("value"));
        responseData.put("Drtflg", body.selectFirst("input[name=Drtflg]").attr("value"));
        responseData.put("DrtflgR", body.selectFirst("input[name=DrtflgR]").attr("value"));
        responseData.put("ad_just_time", body.selectFirst("input[name=ad_just_time]").attr("value"));
        responseData.put("prj1", body.selectFirst("input[name=prj1]").attr("value"));
        responseData.put("sched1", body.selectFirst("input[name=sched1]").attr("value"));
        responseData.put("contnt1", body.selectFirst("input[name=contnt1]").attr("value"));
        responseData.put("memo1", body.selectFirst("input[name=memo1]").attr("value"));
        responseData.put("prj2", body.selectFirst("input[name=prj1]").attr("value"));
        responseData.put("sched2", body.selectFirst("input[name=sched1]").attr("value"));
        responseData.put("contnt2", body.selectFirst("input[name=contnt1]").attr("value"));
        responseData.put("memo2", body.selectFirst("input[name=memo1]").attr("value"));
        responseData.put("prj3", body.selectFirst("input[name=prj1]").attr("value"));
        responseData.put("sched3", body.selectFirst("input[name=sched1]").attr("value"));
        responseData.put("contnt3", body.selectFirst("input[name=contnt1]").attr("value"));
        responseData.put("memo3", body.selectFirst("input[name=memo1]").attr("value"));
        responseData.put("prj4", body.selectFirst("input[name=prj1]").attr("value"));
        responseData.put("sched4", body.selectFirst("input[name=sched1]").attr("value"));
        responseData.put("contnt4", body.selectFirst("input[name=contnt1]").attr("value"));
        responseData.put("memo4", body.selectFirst("input[name=memo1]").attr("value"));
        responseData.put("prj5", body.selectFirst("input[name=prj1]").attr("value"));
        responseData.put("sched5", body.selectFirst("input[name=sched1]").attr("value"));
        responseData.put("contnt5", body.selectFirst("input[name=contnt1]").attr("value"));
        responseData.put("memo5", body.selectFirst("input[name=memo1]").attr("value"));
        responseData.put("dest1", body.selectFirst("input[name=dest1]").attr("value"));
        responseData.put("root1_1", body.selectFirst("input[name=root1_1]").attr("value"));
        responseData.put("cost1", body.selectFirst("input[name=cost1]").attr("value"));
        responseData.put("cnt1_1", body.selectFirst("input[name=cnt1_1]").attr("value"));
        responseData.put("etc1_1", body.selectFirst("input[name=cnt1_1]").attr("value"));
        responseData.put("root1_2", body.selectFirst("input[name=root1_1]").attr("value"));
        responseData.put("dist1", body.selectFirst("input[name=cost1]").attr("value"));
        responseData.put("cnt1_2", body.selectFirst("input[name=cnt1_1]").attr("value"));
        responseData.put("etc1_2", body.selectFirst("input[name=cnt1_1]").attr("value"));
        logger.debug("AccessKin success. 日付:{}", responseData.get("appdate"));
        return responseData;
    }

    private static String requestDump(Connection.Request request) {
        String method = request.method().toString();
        String url = request.url().toString();
        String header = request.multiHeaders().entrySet().stream()
                .map(field -> field.getKey() + ": " + String.join(", ", field.getValue()))
                .collect(Collectors.joining("\n"));
        String body = request.data().stream()
                .map(field -> QueryJoiner.query(field.key(), field.value()))
                .collect(QueryJoiner.joining());

        return new StringJoiner("\n")
                .add(method + " " + url)
                .add(header)
                .add("")
                .add(body.isEmpty() ? "(no body)" : body).toString();
    }

    public Map<String, String> getCookies() {
        return cookies;
    }
}
