package jp.co.bril.altkin.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Request;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.co.bril.altkin.util.Constants;
import jp.co.bril.altkin.util.QueryJoiner;

public class KinClient {
	private Logger logger = LoggerFactory.getLogger(KinClient.class);
	private static final String PATH_LOGIN = "WMU020/WMU020main.php";

	private String id;
	private String password;
	private String caid;
	private String inWork;
	private Map<String, String> cookies = new LinkedHashMap<>();

	/**
	 * コンストラクタ
	 *
	 * @param id ユーザID
	 * @param password パスワード
	 * @param caid 会社ID
	 * @param inWork 出勤時間
	 */
	public KinClient(String id, String password, String caid, String inWork) {
		this.id = id;
		this.password = password;
		this.caid = caid;
		this.inWork = inWork;
		logger.debug("constructor called. id:{}, password:{}, caid:{}, inWork:{}", id, password, caid, inWork);
	}

	/**
	 * 勤怠システムにログインし、出勤時間を登録する。
	 *
	 * @return 登録結果メッセージ
	 * @throws MalformedURLException if the request URL is not a HTTP or HTTPS URL, or is otherwise malformed
	 * @throws HttpStatusException if the response is not OK and HTTP response errors are not ignored
	 * @throws UnsupportedMimeTypeException if the response mime type is not supported and those errors are not ignored
	 * @throws SocketTimeoutException if the connection times out
	 * @throws IOException on error
	 */
	public String executeLogic() throws IOException {
		logger.debug("executeLogic start. id:{}, password:{}, caid:{}, inWork:{}", id, password, caid, inWork);

		// 勤怠システムにログインする
		Login loginLogic = new Login(id, password, caid);
		String userId = loginLogic.login();

		// ログインのユーザIDが取得できなかった場合は、エラーで終了する
		if (StringUtil.isBlank(userId)) {
			logger.error("login failed. id:{}, password:{}, caid:{}", id, password, caid);
			return "ログインに失敗しました。";
		}

		// ログイン結果からCookieを取得する
		Map<String, String> cookies = loginLogic.getCookies();

		// 勤怠登録画面にアクセスする
		AccessKin accessLogic = new AccessKin();
		Map<String, String> inputParameters = accessLogic.access(cookies);

		// 画面上の入力パラメータが取得できなかった場合、エラーで終了する
		if (inputParameters.isEmpty()) {
			logger.error("access failed.");
			return "勤怠登録画面のアクセスに失敗しました。";
		}

		// アクセス時のCookieを取得する
		cookies.clear();
		cookies = accessLogic.getCookies();

		// アクセス時のレスポンス情報の出勤時間を設定する
		inputParameters.put("in_work", inWork);

		// 勤怠情報を登録する
		RegistKin registLogic = new RegistKin(inputParameters);
		registLogic.regist(cookies);

		// ToDo 登録結果判定方法は調査中

		return "勤怠登録に成功しました。";
	}
}
